package org.israelbuitron.demos.mycatalogsonfragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.israelbuitron.demos.mycatalogsonfragments.R;
import org.israelbuitron.demos.mycatalogsonfragments.beans.Person;
import org.israelbuitron.demos.mycatalogsonfragments.fragments.PersonFragment.OnListFragmentInteractionListener;

import java.util.List;

public class PersonRecyclerViewAdapter
        extends RecyclerView.Adapter<PersonRecyclerViewAdapter.ViewHolder>
        implements View.OnClickListener {

    private final List<Person> mData;
    private final OnListFragmentInteractionListener mListener;
    private ViewHolder mHolder;

    public PersonRecyclerViewAdapter(List<Person> data,
                                     OnListFragmentInteractionListener listener) {
        mData = data;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // Set view holder
        mHolder = holder;

        // Set data to views
        mHolder.setPerson(mData.get(position));

        // Set click listener to view
        mHolder.getView().setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onClick(View view) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onListFragmentInteraction(mHolder.getPerson());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        private TextView mPersonNameView;
        private TextView mPersonAgeView;
        private Person mPerson;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPersonNameView = (TextView) view.findViewById(R.id.fragment_person_name_textview);
            mPersonAgeView = (TextView) view.findViewById(R.id.fragment_person_age_textview);
        }

        public Person getPerson() {
            return mPerson;
        }

        public void setPerson(Person person) {
            mPerson = person;
            mPersonNameView.setText(mPerson.getName());
            mPersonAgeView.setText(mPerson.getAge());
        }

        public View getView() {
            return mView;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPersonAgeView.getText() + "'";
        }
    }
}
