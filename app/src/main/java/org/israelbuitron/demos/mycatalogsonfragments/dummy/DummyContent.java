package org.israelbuitron.demos.mycatalogsonfragments.dummy;

import org.israelbuitron.demos.mycatalogsonfragments.beans.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static final List<Person> PEOPLE = new ArrayList<>();
    public static final Map<String, Person> PEOPLE_MAP = new HashMap<>();

    static {
        // Add some sample items.
        addPerson(new Person("Israel", "30"));
        addPerson(new Person("Gris", "28"));
        addPerson(new Person("Moka", "6"));
        addPerson(new Person("Bernarda", "3"));
    }

    private static void addPerson(Person person) {
        PEOPLE.add(person);
        PEOPLE_MAP.put(person.getName(), person);
    }
}
