package org.israelbuitron.demos.mycatalogsonfragments.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.israelbuitron.demos.mycatalogsonfragments.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PersonFormFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PersonFormFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonFormFragment extends Fragment implements View.OnClickListener {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PERSON_NAME = "person_name";
    private static final String ARG_PERSON_AGE = "person_age";

    private String mPersonNameData;
    private String mPersonAgeData;

    private TextInputLayout mPersonNameLayout;
    private TextInputLayout mPersonAgeLayout;
    private EditText mPersonName;
    private EditText mPersonAge;
    private Button mSubmit;

    private OnFragmentInteractionListener mListener;

    public PersonFormFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param personName Parameter 1.
     * @param personAge Parameter 2.
     * @return A new instance of fragment PersonFormFragment.
     */
    public static PersonFormFragment newInstance(String personName, String personAge) {
        // Create fragment
        PersonFormFragment fragment = new PersonFormFragment();

        // Pack parameters for fragment
        Bundle args = new Bundle();
        args.putString(ARG_PERSON_NAME, personName);
        args.putString(ARG_PERSON_AGE, personAge);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set arguments when exist
        if (getArguments() != null) {
            mPersonNameData = getArguments().getString(ARG_PERSON_NAME);
            mPersonAgeData = getArguments().getString(ARG_PERSON_AGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_person_form, container, false);

        // Reference view objects
        mPersonNameLayout = (TextInputLayout) view.findViewById(R.id.person_form_name_layout);
        mPersonAgeLayout = (TextInputLayout) view.findViewById(R.id.person_form_age_layout);
        mPersonName = (EditText) view.findViewById(R.id.person_form_name);
        mPersonAge = (EditText) view.findViewById(R.id.person_form_age);
        mSubmit = (Button) view.findViewById(R.id.person_form_submit);

        // Setting view objects
        mSubmit.setOnClickListener(this);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        boolean hasErrors = false;

        // Validate person name
        if(mPersonName.getText().toString().length()==0){
            // Set error message
            mPersonNameLayout.setError(getContext().getString(R.string.person_form_name_layout_error));
            hasErrors = true;
        }  else {
            // Unset error message
            mPersonNameLayout.setError(null);
        }

        // Validate person age
        if(mPersonAge.getText().toString().length()==0){
            // Set error message
            mPersonAgeLayout.setError(getContext().getString(R.string.person_form_age_layout_error));
            hasErrors = true;
        }  else {
            // Unset error message
            mPersonAgeLayout.setError(null);
        }

        if(!hasErrors) {
            Toast.makeText(getContext(), "Valid data", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
